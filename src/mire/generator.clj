;; vitya
(ns mire.generator)

(def room-descriptions ["A room"])
(def next-room-id (ref 0))

(def start-room {:id 0 
                 :desc "Start room"
                 :ways {}
                 :keys []
                 :safe nil
                 :notes []})
(def two-rooms [{:id -1 :desc "Room 1" :ways {} :keys []} {:id -2 :desc "Room 2" :ways {} :keys []}])

(defn rand-irange [a b] (+ a (rand-int (- b a))))
;;sarkis
(defn get-random-room [] 
    (let [room-desc (rand-nth room-descriptions)
          room-id (dosync (alter next-room-id inc))]
        {:id room-id
         :desc room-desc
         :ways {}
         :keys []
         :safe nil
         :notes []}))

(def all-exits #{:north :south :east :west :up :down})
(defn reverse-direction [dir]
    (case dir
        :north :south
        :south :north
        :east :west
        :west :east
        :up :down
        :down :up))

(defn add-exits [rooms exits]
    (mapv (fn [room] 
             (let [relevant-exits 
                   (filter (fn [[dir to-id from-id]] (= (:id room) from-id)) exits)
                   exit-vect (mapcat (fn [[dir to-id from-id]] [dir to-id]) relevant-exits)]
                  (if (empty? exit-vect) 
                      room 
                      (assoc room :ways 
                       (apply assoc (:ways room) exit-vect)))))
         rooms))
