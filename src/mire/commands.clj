
(ns mire.commands
  (:require [clojure.string :as str]
            [mire.rooms :as rooms]
            [mire.player :as player]
            [mire.count-map :as cm]))

(defn- move-between-refs
  "Move one instance of obj between from and to. Must call in a transaction."
  [obj from to]
  (alter from disj obj)
  (alter to conj obj))

(defn- move-between-count-maps
  "Move one instance of obj between two count-maps from and to. Must call in a transaction."
  [obj from to]
  (alter from cm/cm-remove obj)
  (alter to cm/cm-add obj))

;; Command functions

;;vitua
(defn look
  "Get a description of the surrounding environs and its contents."
  []
  (str (:desc @player/*current-room*) ". Room number " (:id @player/*current-room*) "."
      "\nWays: " (apply list (map
                              (fn [[dir [_ k _]]] [dir (if (empty? @k) "open" "closed")]) 
                              @(:ways @player/*current-room*))) 
      "\n\n"
      (if (= (count @(:gamers @player/*current-room*)) 1) ""
      (str "Gamers in the room: "
        (str/join "," (map #(str %)
                    (disj @(:gamers @player/*current-room*) player/*name*))) 
        "\n"))
      (str "Items: "
        (str/join " " @(:items @player/*current-room*)))
       (if (nil? (:safe @player/*current-room*)) ""
        (case @(first (:safe @player/*current-room*))
          :closed "There is a closed safe here.\n"
          :open "There is an empty safe here.\n"))))

;; ulya
(defn move
  "Give a direction."
  [direction]
  (dosync
   (let [[target-name required-keys _] ((:ways @player/*current-room*) (keyword direction))
         target (@rooms/rooms target-name)]
     (if (or target (= target-name -1))
       (if (empty? @required-keys)
        (if target
          (do
            (doseq [inhabitant (disj @(:gamers @player/*current-room*)
                                player/*name*)]
             (binding [*out* (player/streams inhabitant)]
               (println player/*name* "exited the room.")
               (print player/prompt) (flush)))
            (doseq [inhabitant (disj @(:gamers target)
                                player/*name*)]
             (binding [*out* (player/streams inhabitant)]
               (println player/*name* "entered the room.")
               (print player/prompt) (flush)))
            (move-between-refs player/*name*
                              (:gamers @player/*current-room*)
                              (:gamers target))
            (ref-set player/*current-room* target)
            (look))
          (if (nil? @rooms/game-over-time)
            (do
              (doseq [inhabitant (disj @(:gamers @player/*current-room*)
                                  player/*name*)]
               (binding [*out* (player/streams inhabitant)]
                 (println player/*name* "exited the room.")
                 (print player/prompt) (flush)))
              (doseq [inhabitant (disj (into #{} (keys @player/streams))
                                  player/*name*)]
                (binding [*out* (player/streams inhabitant)]
                  (println "Someone has exited the dungeon! You have 1 minute to find the exit, and you will win if you have more gold than them.")
                  (print player/prompt) (flush)))
              (ref-set rooms/game-over-time (+ (System/currentTimeMillis) 60000))
              (ref-set rooms/current-winner player/*name*)
              (ref-set rooms/winner-gold @player/*gold*)
              (ref-set player/*exited* true)
              (alter (:gamers @player/*current-room*) disj player/*name*)
              "You exited! Now everyone has 1 minute to leave, and the person who makes it in time and has the most gold wins!")
            (if (> @player/*gold* @rooms/winner-gold)
              (do
                (ref-set player/*exited* true)
                (alter (:gamers @player/*current-room*) disj player/*name*)
                (ref-set rooms/current-winner player/*name*)
                (ref-set rooms/winner-gold @player/*gold*)
                "You exited and you have more money than everyone else who did (for now...). Wait around to see who wins!")
              (do
                (ref-set player/*exited* true)
                (alter (:gamers @player/*current-room*) disj player/*name*)
                "You exited! Unfortunately, someone has more money than you, so you're not going to win"))))
        (str "The door is closed. To open it you need the following keys: " (apply list @required-keys)))
      "You can't go that way."))))

;; lesya
(defn grab
  "Pick something up."
  [key]
  (dosync
   (if (rooms/room-contains? @player/*current-room* key)
     (if (and (player/is-key? key) (player/carrying-key?)) 
      (str "You're already carrying a key")
      (do (move-between-count-maps (keyword key)
                            (:items @player/*current-room*)
                            player/*inventory*)
         (str "You picked up the " key ".")))
     (str "There isn't any " key " here."))))

(defn discard
  "Put somekey down that you're carrying."
  [key]
  (dosync
   (if (player/carrying? key)
     (do (move-between-count-maps (keyword key)
                                  player/*inventory*
                                  (:items @player/*current-room*))
         (str "You dropped the " key "."))
     (str "You're not carrying a " key "."))))

;artem
(defn inventory
  "See what you've got."
  []
  (str "You are carrying:\n"
       (str/join "\n" (seq @player/*inventory*))
       "\nYou also have " @player/*gold* " gold."))
;;sasrkis
(defn say
  "Say somekey out loud so everyone in the room can hear."
  [& words]
  (let [message (str/join " " words)]
    (doseq [gamer (disj @(:gamers @player/*current-room*)
                             player/*name*)]
      (binding [*out* (player/streams gamer)]
        (println player/*name* "says:" message)
        (print player/prompt) (flush)))
    (str "You said " message)))
;;kostya
(defn private-message
  "Message to the gamers."
  [gamer & words]
  (let [message (str/join " " words)]
   (binding [*out* (player/streams gamer)]
    (println "Message from" player/*name* message)
    (print player/prompt) (flush))))

(defn remove-one [key coll]
  (let [[n m] (split-with #(not= key %) coll)] (concat n (rest m))))
;; masha
(defn use-key
  "Use key on the door."
  [key door]
  (dosync
    (let [[target-name required-keys who-used] ((:ways @player/*current-room*) (keyword door))]
        (cond 
         (not (player/is-key? key)) (str key " is not a key.")
         (not (player/carrying? key)) (str "You're not carrying a " key ".")
         (not target-name) "There's no door like that."

         (not (some #{(keyword key)} @required-keys)) 
         (str "The " door " door doesn't need a " key ".")

         (@who-used player/*name*)
         "You cannot use more than one key on a single door, ask someone else to do it."

         :else (do 
                (alter player/*inventory* cm/cm-remove (keyword key))
                (alter (second (@(:ways @player/*current-room*) (keyword door)))
                       #(remove-one (keyword key) %))
                (alter ((@(:ways @player/*current-room*) (keyword door)) 2)
                       #(conj % player/*name*))
                (str "You used the " key " on the " door " door."))))))
;;amin
(defn notes
  "Look at the notes."
  []
  (if (empty? (:notes @player/*current-room*)) "There are no notes."
    (if (= (count (:notes @player/*current-room*)) 1)
      (str "The note contains a following number: " 
        (first (:notes @player/*current-room*)))
      (str "The notes contain following numbers: " 
        (:notes @player/*current-room*)))))

(defn safe
  "Try to open the safe."
  [code]
  (dosync
    (let [[status gold codes] (:safe @player/*current-room*)]
        (println "Codes=" codes)
        (cond 
         (nil? (:safe @player/*current-room*)) "There's no safe here."
         (= @status :open) "The safe is already opened."
        
         (not (some #{(str code)} codes)) 
         (str "The code didn't work. Maybe try another one?")
        
         :else (do 
                (alter player/*gold* + gold)
                (ref-set status :open)
                (str "You opened the safe. There was " gold " gold there. You now have " @player/*gold* " gold."))))))

;;ilya
(defn help
  "Show available commands and what they do."
  []
  (str/join "\n" (map #(str (key %) ": " (:doc (meta (val %))))
                      (dissoc (ns-publics 'mire.commands)
                              'execute 'commands))))

;; Command data

(def commands {"move" move,
               "north" (fn [] (move :north)),
               "south" (fn [] (move :south)),
               "east" (fn [] (move :east)),
               "west" (fn [] (move :west)),
               "up" (fn [] (move :up)),
               "down" (fn [] (move :down)),
               "exit" (fn [] (move :exit)),
               "grab" grab,
               "discard" discard,
               "inventory" inventory,
               "look" look,
               "say" say,
               "to" private-message,
               "use" use-key,
               "safe" safe,
               "notes" notes,
               "help" help})

;; Command handling

(defn execute
  "Execute a command that is passed to us."
  [input]
  (if @player/*exited* "You already left the dungeon, you can't do that!"
    (try (let [[command & args] (.split input " +")] 
           
            (apply (commands command) args))
        (catch Exception e
          (.printStackTrace e (new java.io.PrintWriter *err*))
          "You can't do that!"))))
