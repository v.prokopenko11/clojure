;; vitya
(ns mire.player (:require [mire.count-map :as cm] [mire.generator :as gen]))

(def ^:dynamic *current-room*)
(def ^:dynamic *inventory*)
(def ^:dynamic *name*)
(def ^:dynamic *gold*)
(def ^:dynamic *exited*)

(def prompt "Command> ")
(def streams (ref {}))
(def gold-vaults (ref {}))

(defn carrying? [key]
  (cm/has? @*inventory* (keyword key)))

(defn is-key? [key]
  (gen/all-keys-set (keyword key)))

(defn carrying-key? []
  (some #(cm/has? @*inventory* %) gen/all-keys-set))
